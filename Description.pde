//The heat source
//Each frame a few pixels of maximum brightness are placed randomly at the bottom few lines of the screen.

//Heat spread
//Then the image is smoothed. This makes the fire spread as it ages and gives the fire body.
//pixel(x,y) = (pixel(x,y-1) + pixel(x,y+1) + pixel(x-1,y) + pixel(x+1,y))
 //                            -----------------------------------------------------------
   //                                                     4

//Cooling
//The flames are then cooled a little by subtracting a small amount from each pixel.

//Convection
//The image is scrolled up by one pixel. This makes the flames rise.
