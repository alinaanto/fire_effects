PGraphics buf1, buf2;
PImage cooling_map;
float ystart = 0.0;
float col_incrm =0;
float col_b = 0;
float col_g =0;
int w = 600, h = 400;

void setup(){
size(1200, 400);
buf1 = createGraphics(w, height);
buf2 = createGraphics(w, height);
cooling_map= createImage(w, height, RGB);
}

 //<>//

void fire(int rows){
    buf1.beginDraw();
buf1.loadPixels();

for (int x = 0; x < w; x++) {
    for (int j = 0; j < rows; j++) {
      int y = h-(j+1);
      int index = x + y * w;
      buf1.pixels[index] = color(210);
    }
  }
  buf1.updatePixels();
  buf1.endDraw();
}


void draw(){
  fire(2);

buf2.beginDraw();
buf1.beginDraw();
buf1.loadPixels();
buf2.loadPixels();
//////////1///////////
////A ROW OF PIXELS THAT ARE WHITE IN THE BOTTOM 

  cool();
  background(0);
///////2//////////
boolean dr = false;
for (int x= 1; x < w-1; x++){
  for (int y= 1; y < h-1; y++){
      int coord_loc1 = (x+1)+(y)*w;//get the color of pixel
      int coord_loc2 = (x-1)+y*w;
      int coord_loc3 = (x)+(1+y)*w;
      int coord_loc4 = (x)+(y-1)*w;
    color pix1=  buf1.pixels[coord_loc1];
    color pix2=  buf1.pixels[coord_loc2];
    color pix3=  buf1.pixels[coord_loc3];
    color pix4=  buf1.pixels[coord_loc4]; //<>//
    
      int coord_loc = (x)+y*w;
      color pix5=  cooling_map.pixels[coord_loc];
   //make it evaporate
   float grad = (brightness(pix1)+brightness(pix2)+brightness(pix3)+brightness(pix4))/4;
   grad -= brightness(pix5);
      buf2.pixels[coord_loc4] = color(grad, col_g ,col_b);



if(col_b>=255 && !dr){
dr = true;

}else{
if(col_b<=30){
dr = false;
}
if(dr){
   col_b-=1.1;
      col_g+=1.1;

  }  
  if(x%2==0 && !dr){
  col_b+=1.1;
     col_g-=1.1;
}

  }
  
  

} 
  
}
  buf2.updatePixels();
  buf2.endDraw();
  
  PGraphics tmp = buf1;
  buf1 = buf2;
  buf2 = tmp;
  image(buf2,0,0);
 image(cooling_map, w,0);

    buf1.endDraw();
}









void cool(){
  cooling_map.loadPixels();
  float xoff = 0.0; // Start xoff at 0
  float increment = 0.03;
  // For every x,y coordinate in a 2D space, calculate a noise value and produce a brightness value
  for (int x = 0; x <w; x++) {
    xoff += increment;   // Increment xoff 
    float yoff = ystart;   // For every xoff, start yoff at 0
  for (int y = 0; y < h; y++) {
      yoff += increment; // Increment yoff

     //  noiseDetail(60,0.5);
      float n = noise(xoff, yoff);   
      if (col_incrm >255) 
         {col_incrm -=5;}
      float bright = pow(n, 3) * col_incrm;
    // Set each pixel onscreen to a grayscale value

   if((x) <= (w/3)){  
     //change+=10;
      cooling_map.pixels[x+y*w] = color((bright), 0, 0);
    } else{
   if((x) <= (w/2)){
    // change+=10;
      cooling_map.pixels[x+y*w] = color(0, (bright), 0);
    } else{
      
   if((x) <= (w)){
        ///  change+=10;
      cooling_map.pixels[x+y*w] = color(0, 0,  (bright)); //<>//
    } 
    }
    }      
    }    
  }
   col_incrm+=15;
  cooling_map.updatePixels();
  ystart += increment;
 
}
